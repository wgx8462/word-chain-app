import React, {useState} from 'react';

const WordChains = ({words}) => {
    const [inText, setInText] = useState('')
    const [textValue, setTextValue] = useState([])

    const handleInputChange = (e) => {
        setInText(e.target.value)
    }

    const addToText = () => {
        if (textValue.length < 20) {
            if (textValue.indexOf(inText) === -1) {
                if (textValue.length === 0) setTextValue([...textValue, inText])
                else if (textValue[textValue.length - 1].charAt(textValue[textValue.length - 1].length - 1) === inText.charAt(0)) setTextValue([...textValue, inText])
                else alert("시작단어가 다르다 빠가사리니")
            } else alert("중복된 단어 입니다. 다시 입력해라!!")
        }
        setInText('')
        console.log(textValue)
    }


    return (
        <div>
            <h2>끝말 잇기를 해보자</h2>
            <input type='text' value={inText} onChange={handleInputChange}></input>
            <button onClick={addToText}>등록</button>

            <div>
                {textValue.map((textVal, index) => (
                    <div key={index}>
                        <p>{index + 1 + "."} {textVal}</p>
                    </div>
                ))}
                {(textValue.length === 20) ? <button onClick={() => window.location.reload()}>다시시작</button> : ''}
            </div>
        </div>
    )
}

export default WordChains;