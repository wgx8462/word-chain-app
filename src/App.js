import './App.css';
import WordChains from "./components/WordChains";

function App() {
  return (
    <div className="App">
      <WordChains />
    </div>
  );
}

export default App;
